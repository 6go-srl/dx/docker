FROM php:8.4-fpm-alpine

LABEL maintainer="6GO S.r.l. <opensource@6go.it>" \
	software="php" \
	org.label-schema.name="6go/dx-docker" \
	org.label-schema.description="Docker image for internal usage on development" \
	org.label-schema.build-date=$BUILD_DATE \
	org.label-schema.schema-version="8.4" \
	org.label-schema.vcs-url="https://gitlab.com/6go/dx/docker"

ARG BUILD_DATE

ENV COMPOSER_ALLOW_SUPERUSER=1

# We can decide to enable or disable opcache in runtime build
# for example in development it's useful to keep this disable
ENV OPCACHE_ENABLE=1
ENV OPCACHE_VALIDATE_TIMESTAMPS=0
ENV OPCACHE_JIT_DEBUG=0

# xDebug mode should be set off in production
ENV XDEBUG_MODE=off
ENV XDEBUG_CLIENT_HOST=localhost
ENV XDEBUG_CLIENT_PORT=9003

# Pcov should be set off in production
ENV PCOV_ENABLED=0

# Copy other .ini files after the installation process
COPY ./conf/www.conf /usr/local/etc/php-fpm.d/www.conf
COPY ./conf/zz-docker.conf /usr/local/etc/php-fpm.d/zz-docker.conf
COPY ./ini/post-size.ini "$PHP_INI_DIR/conf.d/99-post-size.ini"
COPY ./ini/opcache.ini "$PHP_INI_DIR/conf.d/99-opcache.ini"
COPY ./ini/xdebug.ini "$PHP_INI_DIR/conf.d/99-xdebug.ini"
COPY ./ini/pcov.ini "$PHP_INI_DIR/conf.d/99-pcov.ini"

# Add composer
COPY --from=registry.gitlab.com/6go/dx/docker/composer:latest /usr/bin/composer/ /usr/local/bin/composer

# Add installer
COPY --from=registry.gitlab.com/6go/dx/docker/composer:latest /usr/local/bin/install-php-extensions /usr/local/bin/install-php-extensions

##
# Imagick is disabled until further notice
# SEE https://github.com/Imagick/imagick/pull/641
##

RUN apk add git make ffmpeg --no-cache \
    && IPE_ICU_EN_ONLY=1 IPE_GD_WITHOUTAVIF=1 install-php-extensions \
        bcmath \
        calendar \
        exif \
        excimer \
        gd \
        intl \
        opcache \
        pcntl \
        # pcov \ Check https://github.com/krakjoe/pcov/pull/111¦
        pdo_mysql \
        redis \
        xdebug \
        zip \
    && mkdir -p /var/www \
    && chown -Rf www-data:www-data /var/www \
    && php -v \
    && composer \
    && rm -rf /usr/share/php /tmp/* /var/cache/apk/*

# Prepare the workdir
WORKDIR /var/www

CMD ["php-fpm", "--nodaemonize"]
