#!/usr/bin/env bash
stripe --api-key ${STRIPE_SECRET}
stripe login -i

# It uses the nginx container name!
stripe listen --forward-to ${NGINX_CONTAINER}/webhooks/stripe
