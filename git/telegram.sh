#!/bin/bash
set -eo pipefail

# Configuration - Values should come from GitLab CI environment variables
CHAT_ID="-1001590459815"
MESSAGE_THREAD_ID=1247

# Validate required environment variables
required_vars=(
  "XGO_TG_RELEASE_BOT"
  "CI_PROJECT_ID"
  "GROUP_ACCESS_TOKEN"
)

for var in "${required_vars[@]}"; do
  if [[ -z "${!var}" ]]; then
    echo "ERROR: Required environment variable $var not set" >&2
    exit 1
  fi
done

# Additional validation for project ID format
if ! [[ "$CI_PROJECT_ID" =~ ^[0-9]+$ ]]; then
  echo "ERROR: Invalid CI_PROJECT_ID format: must be numeric" >&2
  exit 1
fi

# Function to send message to Telegram with proper error handling
send_telegram_message() {
  local text="$1"

  local response
  response=$(curl -s -X POST \
    "https://api.telegram.org/bot${XGO_TG_RELEASE_BOT}/sendMessage" \
    -d "chat_id=${CHAT_ID}" \
    -d "message_thread_id=${MESSAGE_THREAD_ID}" \
    -d "text=${text}" \
    -d "parse_mode=MarkdownV2" \
    -d "disable_notification=true") || {
    echo "ERROR: Telegram API request failed" >&1
    exit 1
  }

  local ok=$(jq -r '.ok' <<< "$response")
  if [[ "$ok" != "true" ]]; then
    local error_msg=$(jq -r '.description' <<< "$response")
    echo "ERROR: Telegram API returned error: $error_msg" >&2
    exit 1
  fi
}

# Unified function to make GitLab API calls
gitlab_api() {
  local endpoint="$1"
  local response
  response=$(curl -s -w "\n%{http_code}" -H "PRIVATE-TOKEN: ${GROUP_ACCESS_TOKEN}" \
    "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/${endpoint}") || {
    echo "ERROR: Failed to connect to GitLab API" >&2
    exit 1
  }

  # Split response into body and status code
  local http_code=$(echo "$response" | tail -n1)
  local body=$(echo "$response" | sed '$d')

  if [[ $http_code -ne 200 ]]; then
    echo "ERROR: GitLab API request failed (HTTP $http_code)" >&2
    exit 1
  fi

  if ! jq -e . >/dev/null 2>&1 <<< "$body"; then
    echo "ERROR: Invalid JSON response from GitLab API" >&2
    exit 1
  fi

  echo "$body"
}

# Function to format message with proper MarkdownV2 escaping
format_message() {
  local project_name="$1"
  local project_namespace="$2"
  local tag_name="$3"
  local description="$4"
  
  # Replace markdown headers (###) with bold formatting (**)
  description=$(echo "$description" | sed 's/### \(.*\)/*\1*/g')
  
  # Simplified version - let's escape fewer characters first
  local message="#${project_namespace} *${project_name} ${tag_name}* %0A%0A ${description}"
  echo "$message" | sed 's/[#.-]/\\&/g'
}


# Main execution flow
main() {
  echo "Starting release notification process for project ID ${CI_PROJECT_ID}"

  # Get project information
  echo "Fetching project info..."
  local project_info
  project_info=$(gitlab_api "") || exit 1
  local project_name=$(jq -r '.name' <<< "$project_info")
  local project_namespace=$(jq -r '.namespace.name' <<< "$project_info")

  # Get latest release
  echo "Fetching release info..."
  local releases
  releases=$(gitlab_api "releases") || exit 1
  local latest_release=$(jq -r '.[0]' <<< "$releases")
  local tag_name=$(jq -r '.tag_name' <<< "$latest_release")
  local description=$(jq -r '.description' <<< "$latest_release")

  # Format and send message
  echo "Creating notification message..."
  local message=$(format_message \
    "$project_name" \
    "$project_namespace" \
    "$tag_name" \
    "$description")

  echo "Sending message to Telegram..."
  send_telegram_message "$message"

  echo "Notification sent successfully!"
}

# Execute main function
main