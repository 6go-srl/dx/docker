#!/bin/bash

## Debug
# CI_PROJECT_URL="https://gitlab.com/6go/dx/docker"
##
URL="${CI_PROJECT_URL}/-/commit"

increment_version() {
    echo "Executing git describe --abbrev=0 --tags"
    # Get the latest tag
    OLD_VERSION=$(git describe --abbrev=0 --tags 2> /dev/null || echo "NO_TAGS_DETECTED")

    # Skip logic for first tag
    if [[ "$OLD_VERSION" == "NO_TAGS_DETECTED" ]]; then
        NEW_VERSION="v0.0.1"
        echo "Skipping check for ${NEW_VERSION}"
        return
    fi

    # Initialize version parts
    IFS='.' read -r -a PARTS <<< "$OLD_VERSION"
    MAJOR=${PARTS[0]:1}
    MINOR=${PARTS[1]}
    PATCH=${PARTS[2]}

    # Check commit log for specific keywords
    if git log --pretty=format:"%s%n%b" $OLD_VERSION..HEAD | grep -q "breaking change:"; then
        # If "breaking change" is found, increment major version
        ((MAJOR++))
        MINOR=0
        PATCH=0
    elif git log --pretty=format:"%s" $OLD_VERSION..HEAD | grep -q "feat:"; then
        ((MINOR++))
        PATCH=0
    else
        ((PATCH++))
    fi

    # Construct the new version string
    NEW_VERSION="v$MAJOR.$MINOR.$PATCH"

    echo "Creating tag: ${NEW_VERSION}"
}

create_tag() {
    echo "Create tag - chore(release): $NEW_VERSION"
    git switch master
    git tag -a "$NEW_VERSION" -m "chore(release): $NEW_VERSION"
    git push --tags https://root:$GROUP_ACCESS_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git HEAD:master
    echo "Created tag $NEW_VERSION"
}

create_master_commit() {
    echo "Create master commit" 
    git switch master
    git add -A
    git commit -m "chore(release): $NEW_VERSION"
    git push -o ci.skip https://root:$GROUP_ACCESS_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git HEAD:master
    echo "Master branch has been updated"
}

backmerge() {
    echo "Backmerge to $CI_DEFAULT_BRANCH - chore(release): $NEW_VERSION"
    git switch $CI_DEFAULT_BRANCH
    git fetch --all
    git merge master --commit --no-ff -m "chore(release): $NEW_VERSION"
    git push -o ci.skip https://root:$GROUP_ACCESS_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git HEAD:$CI_DEFAULT_BRANCH
    git switch master
    echo "Backmerged to $CI_DEFAULT_BRANCH done"
}

update_changelog() {
  MERGE_COMMIT_MESSAGE="Merge branch"

  echo "" >> CHANGELOG.md
  echo "## $NEW_VERSION ($(date +'%Y-%m-%d'))" >> CHANGELOG.md

  # https://www.conventionalcommits.org/en/v1.0.0/#specification
  categories=(
    "feat:### Added"
    "chore,ci,doc,style,refactor,perf,test:### Changed"
    "fix:### Fixed"
  )

  if [[ "$OLD_VERSION" == "NO_TAGS_DETECTED" ]]; then
    echo "Generating changelog for first release"

    IFS=':' read -r -a fields <<< "$category"
    prefixes="${fields[0]}"
    heading="${fields[1]}"
    grep_args="$(echo "$prefixes" | sed 's/,/\\|/g')"

    # For each category we have defined about
    for category in "${categories[@]}"; do
      # Grab Prefixes and Heading
      IFS=':' read -r -a fields <<< "$category"
      prefixes="${fields[0]}"
      heading="${fields[1]}"
      # Format prefixes for grep
      grep_args="$(echo "$prefixes" | sed 's/,/\\|/g')"
      # If Git log has one of the prefix in exam
      if git log --pretty=oneline --invert-grep --grep="$MERGE_COMMIT_MESSAGE" | grep -e "\($grep_args\):" | grep -q .; then
          # Add a blank line to changelog file
          echo "" >> CHANGELOG.md
          # Add the heading 
          echo "$heading" >> CHANGELOG.md
          # For each prefix
          for prefix in $(echo "$prefixes" | tr ',' '\n'); do
            # If the git log has the single prefix
            if git log --pretty=oneline --invert-grep --grep="$MERGE_COMMIT_MESSAGE" | grep -e "$prefix:" | grep -q .; then
              # Add a blank line to changelog file
              echo "" >> CHANGELOG.md
              # Write the changelog
              git log --pretty=format:"  - [%s]($URL/%H)" --invert-grep --grep="$MERGE_COMMIT_MESSAGE" | grep -e "$prefix:" | sed 's/^..//' >> CHANGELOG.md
            fi
          done
      fi
    done

  else
    echo "Generating changelog for ${NEW_VERSION}"

    for category in "${categories[@]}"; do
      IFS=':' read -r -a fields <<< "$category"
      prefixes="${fields[0]}"
      heading="${fields[1]}"
      grep_args="$(echo "$prefixes" | sed 's/,/\\|/g')"
      if git log --pretty=oneline "$OLD_VERSION"..HEAD --invert-grep --grep="$MERGE_COMMIT_MESSAGE" | grep -e "\($grep_args\):" | grep -q .; then
          echo "" >> CHANGELOG.md
          echo "$heading" >> CHANGELOG.md
          for prefix in $(echo "$prefixes" | tr ',' '\n'); do
            if git log --pretty=oneline "$OLD_VERSION"..HEAD --invert-grep --grep="$MERGE_COMMIT_MESSAGE" | grep -e "$prefix:" | grep -q .; then
              echo "" >> CHANGELOG.md
              git log --pretty=format:"  - [%s]($URL/%H)" "$OLD_VERSION"..HEAD --invert-grep --grep="$MERGE_COMMIT_MESSAGE" | grep -e "$prefix:" | sed 's/^..//' >> CHANGELOG.md
            fi
          done
      fi
    done
  fi
}

# Main script
git clean -fdx
git switch master
git fetch --all --tags
increment_version
update_changelog
create_master_commit
backmerge
create_tag

echo "Automation complete."
